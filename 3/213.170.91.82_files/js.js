var cursorX = 227;
var cursorY = 0;
var count = 0;
var RowCount = 18;
var MaxCount = RowCount*RowCount;

document.onmousemove = function(e){
	cursorY = e.clientX;
	cursorX = e.clientY;
}
DrawField2(RowCount);
var connection = new WebSocket('ws://213.170.91.82:8080', 'ws');

connection.onopen = function () {
	connection.send(cursorX+" "+cursorY);
	var timer = setInterval(function(){
		connection.send(cursorX+" "+cursorY);
	}, 250)
};

connection.onmessage = function(e) {
	var fileReader = new FileReader();
	fileReader.onload = function() {
		var result = Uint8ArrayToArrayUint6(new Uint8Array(this.result));
        Draw(result);
        document.getElementById("text").innerHTML = cursorX + " | " + cursorY; //result.join(', ') + '| ';
	};
	fileReader.readAsArrayBuffer(e.data)
};

connection.onerror = function(error) {
};
	
connection.onclose = function() {
};

function Uint8ArrayToArrayUint6(bit8)
{
	var bit6 = []
    var tmp_bit6 = 0, tmp_bits = 0
    offset = 2 
    for (var i=0; i<bit8.length; i++) {
        tmp_bit6 = (bit8[i] >> offset) 
        tmp_bit6 = ((tmp_bit6 | tmp_bits) & 63);
        offset = ((offset - 6 + 8) % 8);
        tmp_bits = ((bit8[i] << (8 - offset)) & 63)
        bit6.push(tmp_bit6)
        if(offset === 0)
            i=i-1;
    }
    return bit6
}

function SetNull()
{
    var pixels = document.getElementsByClassName("pixel")
    for(var i = 0; i < pixels.length; i++)
    {
        pixels[i].style.backgroundColor = "rgba(0,0,0,0)";
    }
}
function Draw(r)
{
    SetNull();
    for(var k = 0; k < r.length; k++)
    {
        var id = r[k];
        var st = "rgba(0,0,0,"+ (Math.round((id / 100) * 100)) / 100 + ")";
        document.getElementById("c" + (k+1)).style.backgroundColor = st;
    }     
    
}
function DrawField(size)
{
    var r = 1;
    var html_data = "";
    var area_sz = (size*size);
    for(var i = size; i >= 1; i--)
    {
        html_data += "<div class=\"rows\" id=\"r" + (r++) + "\">\n";
        for(var j = i; j <= (area_sz - size + i); j += 18)
        {
            html_data += "<div class=\"pixel\" id=\"c" + j + "\"></div>\n";
        }
        html_data += "</div>";
    }
    document.getElementById("field").innerHTML = html_data;
}
function DrawField2(size)
{
    var r = 1;
    var html_data = "";
    var area_sz = (size*size);
    for(var i = 1; i <= size; i++)
    {
        html_data += "<div class=\"rows\" id=\"r" + (r++) + "\">\n";
        for(var j = size * (i - 1); j <= (size * (i - 1) + size); j++)
        {
            html_data += "<div class=\"pixel\" id=\"c" + j + "\"></div>\n";
        }
        html_data += "</div>";
    }
    document.getElementById("field").innerHTML = html_data;
}