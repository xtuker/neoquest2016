function gcd_ex([BigInt]$a,[BigInt]$b)
{
    
    if($b -eq 0)
    {
       return @{x=1;y=0;d=$a}    
    }
    
    [BigInt]$x1 = 0
    [BigInt]$x2 = 1
    [BigInt]$y1 = 1
    [BigInt]$y2 = 0
    
    while($b -gt 0)
    {
        $q = $a / $b
        $r = $a - $q * $b
        $x = $x2 - ($q * $x1)
        $y = $y2 - ($q * $y1)
        $a = $b
        $b = $r
        $x2 = $x1
        $x1 = $x
        $y2 = $y1
        $y1 = $y
    }
    
    return @{x=$x2;y=$y2;d=$a}
}

function inverse([BigInt]$a,[BigInt]$n)
{
    $gcd = gcd_ex $a $n
    if($gcd.d -eq 1)
    {
        return ($gcd.x % $n + $n) % $n
    }
    else
    {
        return 0
    }
    
}

function get-root([BigInt]$number, [int]$power)
{
    $ret = [BigInt]::Pow(2,([bigint]::Log($number, 2) / $power))
    
    $test = [BigInt]::Pow($ret, $power)
    
    $delta = [BigInt]::Pow(10, $ret.ToString().Length-2)

    while($delta -ne 0)
    {
        if($test -eq $number)
        {
            return $ret
        }
        
        $ret += $delta
        $test = [BigInt]::Pow($ret, $power) 
                
        switch([BigInt]::Compare($test, $number)) 
        {
            -1 { 
                if($delta -lt -1)
                {
                    $delta /= 10
                    $delta = [BigInt]::Negate($delta)
                }
                if($delta -eq -1)
                {
                    $delta = 0
                }
                Write-Debug ("l_{0}" -f $delta.ToString())
                break
            }
            0 {
                Write-Debug ("e_{0}" -f $delta.ToString())
                return $ret
            }
            1 { 
                if($delta -gt 1)
                {
                    $delta /= 10
                    $delta = [BigInt]::Negate($delta)
                }
                if($delta -eq 1)
                {
                    $delta = 0
                }
                Write-Debug ("g_{0}" -f $delta.ToString())
                break 
            }
        }
        
        
    }
    if($test -ne $number) { Write-Error ("`n{0}`n`n{1}" -f $test.ToString(), $number.ToString()) }
    
    return $ret
}

$e = 3
$ni = [BigInt]::Parse("770208589881542620069464504676753940863383387375206105769618980879024439269509554947844785478530186900134626128158103023729084548188699148790609927825292033592633940440572111772824335381678715673885064259498347"), [BigInt]::Parse("106029085775257663206752546375038215862082305275547745288123714455124823687650121623933685907396184977471397594827179834728616028018749658416501123200018793097004318016219287128691152925005220998650615458757301"), [BigInt]::Parse("982308372262755389818559610780064346354778261071556063666893379698883592369924570665565343844555904810263378627630061263713965527697379617881447335759744375543004650980257156437858044538492769168139674955430611")

$Ci = [BigInt]::Parse("258166178649724503599487742934802526287669691117141193813325965154020153722514921601647187648221919500612597559946901707669147251080002815987547531468665467566717005154808254718275802205355468913739057891997227"), [BigInt]::Parse("82342298625679176036356883676775402119977430710726682485896193234656155980362739001985197966750770180888029807855818454089816725548543443170829318551678199285146042967925331334056196451472012024481821115035402"), [BigInt]::Parse("22930648200320670438709812150490964905599922007583385162042233495430878700029124482085825428033535726942144974904739350649202042807155611342972937745074828452371571955451553963306102347454278380033279926425450")

$M = 1; 
$ni | %{$M *= $_}                                 # M = ni[0] * ... * ni[n]
$Mi = $ni | %{ $M / $_ }                          # Mi[i] = M / ni[i]
$M_i = 0..2 | %{ inverse $Mi[$_] $ni[$_] }        # M_i[i] = (1 / Mi[i]) mod ni[i]
$x = 0; 
0..2 | %{ $x += ($Ci[$_] * $Mi[$_] * $M_i[$_])}   # $x = (Ci[0] * Mi[0] * M_i[0]) + ... + (Ci[n] * Mi[n] * M_i[n])
$x %= $M

$y = get-root $x 3

$text = [system.Text.Encoding]::ASCII.GetString($y.ToByteArray())

$ret_text = ""; ($text.Length-1) .. 0 | %{$ret_text += $text[$_]}; $ret_text

